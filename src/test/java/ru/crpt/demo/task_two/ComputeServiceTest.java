package ru.crpt.demo.task_two;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.crpt.demo.task_two.service.ComputeService;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author : Anton Arefyev e-mail: anthon.arefyev@gmail.com
 */
public class ComputeServiceTest {

    private ComputeService<SomeKey, Integer> computeService;
    private static SomeKey someKeyDefault;

    @BeforeAll
    public static void init(){

        someKeyDefault = new SomeKey("testCase", 1234556765);
    }

    @Test
    public void computeTest() throws ExecutionException, InterruptedException {
        computeService = new ComputeService<>();
        Integer checkResult = computeCall(someKeyDefault);
        Future<Integer> fv = computeService.compute(someKeyDefault, ComputeServiceTest::computeCall);
        assertEquals(checkResult, fv.get());
        assertEquals(1, computeService.getMap().size());
        Future<Integer> fv2 = computeService.compute(someKeyDefault, ComputeServiceTest::computeCall);
        assertEquals(checkResult, fv2.get());
        assertEquals(1, computeService.getMap().size());
    }

    // unsafe-thread method
    private static Integer computeCall(SomeKey someKey){

        Integer i = 0;
        while (i < someKey.getData()){
            i++;
        }
        return i;
    }
}
