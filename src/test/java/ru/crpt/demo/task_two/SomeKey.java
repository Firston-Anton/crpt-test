package ru.crpt.demo.task_two;

import lombok.Builder;
import lombok.Data;

/**
 * @author : Anton Arefyev e-mail: anthon.arefyev@gmail.com
 */
@Data
@Builder
public class SomeKey{

    private String name;
    private Integer data;


}
