package ru.crpt.demo.task_one.controller;



import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.crpt.demo.CrptDemoApplication;
import ru.crpt.demo.task_one.pojo.Document;
import ru.crpt.demo.task_one.pojo.Product;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.*;

/**
 * @author : Anton Arefyev e-mail: anthon.arefyev@gmail.com
 */
@SpringBootTest(classes = CrptDemoApplication.class)
@AutoConfigureMockMvc
public class DocumentControllerTest {

    private static final String FULL_PATH = "/document/product";

    @Autowired
    public DocumentControllerTest(MockMvc mockMvc, ObjectMapper objectMapper){
        this.mockMvc = mockMvc;
        this.objectMapper = objectMapper;
    }

    private final MockMvc mockMvc;
    private final ObjectMapper objectMapper;

    private Document document;

    @Test
    public void positiveCallApi() throws Exception {
        document = new Document(
                "id_123456", "id_456789",
                Collections.singleton(
                        new Product("milk", "id_1234567890")
                ));
        mockMvc.perform(post(FULL_PATH)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(document))
        ).andExpect(status().isOk())
        .andDo(print());
    }

    @Test
    public void negativeCallApiEmptyBody() throws Exception {
        document = null;
        mockMvc.perform(post(FULL_PATH)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(document)))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("message", equalTo("Conversion attempt fails")))
        .andDo(print());
    }

    @Test
    public void negativeCallApiIncorrectFormat() throws Exception {
        document = new Document(
                "id_1", "456789098767",
                Collections.singleton(
                        new Product("", "id_123456")
                ));
        mockMvc.perform(post(FULL_PATH)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(document)))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("message", equalTo("Validation failed")))
        .andExpect(jsonPath("details", hasSize(4)))
        .andDo(print());
    }
}
