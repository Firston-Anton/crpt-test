package ru.crpt.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class CrptDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrptDemoApplication.class, args);
    }

}
