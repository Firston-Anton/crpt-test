package ru.crpt.demo.task_one.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author : Anton Arefyev e-mail: anthon.arefyev@gmail.com
 */
@ApiModel(description = "Description of the product")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {

    private static final long serialVersionUID = -4667001416300388008L;

    @ApiModelProperty(name = "name", value = "Product`s name", required = true, example = "milk")
    @JsonProperty(value = "name")
    @NotEmpty(message = "Product`s name can`t be empty")
    private String name;

    @ApiModelProperty(name = "code", value = "Product`s identifier `code`", required = true, example = "id_0123456789")
    @JsonProperty(value = "code")
    @NotNull(message = "Product`s identifier `code` can`t be null")
    @Size(min = 13, max = 13, message = "Identifier `code` must be 13 symbols")
    private String codeId;
}
