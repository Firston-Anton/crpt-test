package ru.crpt.demo.task_one.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author : Anton Arefyev e-mail: anthon.arefyev@gmail.com
 */
@ApiModel(description = "Description of the input document")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Document implements Serializable {

    private static final long serialVersionUID = -7822130540398400844L;

    @ApiModelProperty(name = "seller", value = "Identifier `seller`", required = true, example = "123534251")
    @JsonProperty(value = "seller")
    @NotNull(message = "Identifier `seller` can`t be null")
    @Size(min = 9, max = 9, message = "Identifier `seller` must be 9 symbols")
    private String sellerId;

    @ApiModelProperty(name = "customer", value = "Identifier `customer`", required = true, example = "648563524")
    @JsonProperty(value = "customer")
    @NotNull(message = "Identifier `customer` can`t be null")
    @Size(min = 9, max = 9, message = "Identifier `customer` must be 9 symbols")
    private String customerId;

    @ApiModelProperty(name = "products", value = "Product`s collection", required = true)
    @JsonProperty(value = "products")
    @Valid
    @NotEmpty(message = "Product`s collection can`t be empty")
    private Set<Product> productSet;

}
