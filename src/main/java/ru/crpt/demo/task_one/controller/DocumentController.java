package ru.crpt.demo.task_one.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.crpt.demo.task_one.common.GenericError;
import ru.crpt.demo.task_one.pojo.Document;

import javax.validation.Valid;
import java.net.HttpURLConnection;


/**
 * @author : Anton Arefyev e-mail: anthon.arefyev@gmail.com
 */
@Api(value = "Service product range", tags = Tags.PRODUCT_RANGE)
@RestController
@RequestMapping("/document")
public class DocumentController {

    @ApiOperation(value = "Product Document Processing")
    @ApiResponses(value = {
            @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Document processed successfully"),
            @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "Document Validation Errors", response = GenericError.class),
            @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Unhandled exception")
    })
    @PostMapping("/product")
    public ResponseEntity<Document> acceptProducts(@Valid @RequestBody Document document) {
        return ResponseEntity.ok(document);
    }
}
