package ru.crpt.demo.task_one.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

/**
 * @author : Anton Arefyev e-mail: anthon.arefyev@gmail.com
 */
@ApiModel(description = "Generic error")
@Data
@Builder
public class GenericError implements Serializable {

    private static final long serialVersionUID = -4935962423486474937L;

    @ApiModelProperty(value = "Error uuid", name = "uuid", example = "fee8a33d-bd5b-4153-9207-12b8166be682")
    @JsonProperty(value = "uuid")
    @NonNull
    private final UUID uuid;

    @ApiModelProperty(name = "message", value = "Error message", example = "Some message")
    @JsonProperty(value = "message")
    @NonNull
    private final String message;

    @ApiModelProperty(name = "details", value = "Error details")
    @JsonProperty(value = "details")
    @Builder.Default
    private final Set<String> details;
}
