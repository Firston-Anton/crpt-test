package ru.crpt.demo.task_one.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author : Anton Arefyev e-mail: anthon.arefyev@gmail.com
 */
@Slf4j
@RestControllerAdvice
public class ControllerExceptionHandler {

    private final static String ERROR_IN_VALIDATION = "Validation failed";
    private final static String ERROR_IN_CONVERSION = "Conversion attempt fails";

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<GenericError> handler(MethodArgumentNotValidException e) {
        UUID uuid = UUID.randomUUID();
        log.error(String.valueOf(uuid), e);
        return ResponseEntity.badRequest()
                .body(GenericError.builder()
                        .uuid(uuid)
                        .message(ERROR_IN_VALIDATION)
                        .details(prepareResponse(e.getBindingResult()))
                        .build());
    }

    @ExceptionHandler(HttpMessageConversionException.class)
    public ResponseEntity<GenericError> handler(HttpMessageConversionException e) {
        UUID uuid = UUID.randomUUID();
        log.error(String.valueOf(uuid), e);
        return ResponseEntity.badRequest()
                .body(GenericError.builder()
                    .uuid(uuid)
                    .message(ERROR_IN_CONVERSION)
                    .details(Collections.singleton(e.getMessage()))
                    .build()
                );
    }

    private Set<String> prepareResponse(BindingResult bindingResult) {
            return bindingResult.getAllErrors()
                    .stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.toSet());
    }
}
