package ru.crpt.demo.task_two.service;

import lombok.Getter;
import lombok.NonNull;

import java.util.Map;
import java.util.concurrent.*;
import java.util.function.Function;

/**
 * @author : Anton Arefyev e-mail: anthon.arefyev@gmail.com
 */
public class ComputeService<K, V>{

    private final ExecutorService executorService = Executors.newCachedThreadPool();
    @Getter
    private final Map<K, Future<V>> map = new ConcurrentHashMap<>();

    /**
     *  Calculations should be short and simple. Method faster than compute1
     */
    public Future<V> compute(@NonNull K k, @NonNull Function<K, V> f){
        return map.computeIfAbsent(k, mappingFunction(f));
    }

    private Function<K, Future<V>> mappingFunction(Function<K, V> f){
        return k -> CompletableFuture.supplyAsync(() -> f.apply(k), executorService);
    }
}
